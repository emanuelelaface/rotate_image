import sys
import numpy
import cv2
from scipy import ndimage
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QPixmap, QImage
from PyQt5.QtCore import Qt

class GUI(QWidget):
	def __init__(self):
		super().__init__()
		self.initGUI()
		self.createEvents()

	def initGUI(self):
		self.image_label = QLabel(self)
		self.open_button = QPushButton('Open', self)
		self.angle_slider = QSlider(Qt.Horizontal, self)
		self.angle_slider.setRange(-360, 360)
		self.angle_slider.setEnabled(False)

		self.image = (numpy.ones((900,540,4))*155).astype(numpy.uint8)

		grid = QGridLayout()
		grid.setSpacing(5)
		grid.addWidget(self.open_button, 0, 0)
		grid.addWidget(self.image_label, 1, 0)
		grid.addWidget(self.angle_slider, 2, 0)

		self.setLayout(grid)
		self.setWindowTitle('Batman Rotation')
		self.move(300, 200)
		self.show()

	def createEvents(self):
		self.open_button.clicked.connect(self.loadImage)
		self.angle_slider.valueChanged.connect(self.rotateImage)

	def paintEvent(self, e):
		image = cv2.resize(self.image,(900, 540))
		image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
		image = QImage(image, 900, 540, QImage.Format_RGB888)
		self.image_label.setPixmap(QPixmap.fromImage(image))

	def loadImage(self):
		file_name = QFileDialog.getOpenFileName(self, 'Select Image', '.')
		try:
			self.image_array = cv2.imread(file_name[0]).astype(numpy.uint8)
			self.image = self.image_array
			self.angle_slider.setValue(0)
			self.angle_slider.setEnabled(True)
		except:
			None

	def rotateImage(self, angle):
		self.image = ndimage.rotate(self.image_array, angle, reshape=False)
		
if __name__ == '__main__':
	app = QApplication(sys.argv)
	ex = GUI()
	sys.exit(app.exec_())
