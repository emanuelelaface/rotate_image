import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from scipy import ndimage

def rotateImg(arrayImage, theta):
    return ndimage.rotate(arrayImage, theta, reshape=False)

def main():
    image = Image.open('batman.png').convert('L')
    angle = 45
    rotatedImg = rotateImg(np.asarray(image), angle)
    plt.imsave(arr=rotatedImg, fname='rotated.png', cmap = plt.cm.Greys_r)

if __name__ == "__main__":
    main()
